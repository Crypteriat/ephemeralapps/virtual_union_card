# Virtual Anonymous Membership

This is a pilot project application for the ephemeral link and identity system. The goal is to provide an identity with attestation for members of a
tech workers union. The membership can be made ephemeral so that members can virtually identify themselves. Union membership provides benefits through
the progressive web app such that users can purchase retirement, anonymously vote, and build petitions for union inititatives.

